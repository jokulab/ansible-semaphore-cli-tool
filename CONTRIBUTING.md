# Contributing to Ansible Semaphore Go Library

We welcome contributions to the Ansible Semaphore Go Library. To contribute, please follow these guidelines:

## Reporting Issues

If you encounter a bug or have a feature request, please open an issue on the [issue tracker](https://gitlab.com/jokulab/ansible-semaphore-go-library/-/issues).

## Making Pull Requests

1. Fork the repository.
2. Create a feature branch from the `main` branch.
3. Make your changes and commit them with clear and concise messages.
4. Ensure that your code follows the project's coding standards.
5. Create a pull request to the `main` branch of this repository.

## Code Standards

- Follow the Go community's [code style](https://golang.org/doc/effective_go.html) guidelines.
- Write meaningful comments and documentation for your code.
- Run tests to ensure your changes do not break existing functionality.

Thank you for contributing to the Ansible Semaphore Go Library!
